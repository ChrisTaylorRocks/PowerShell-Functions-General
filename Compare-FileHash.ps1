﻿function Compare-FileHash {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$True,Position=1)]
        $File1,	        
        [Parameter(Mandatory=$True,Position=2)]
        $File2
    )
    #Used by:
    ##CryptoMonitor
    
    $md5 = new-object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider

    $Original = [System.BitConverter]::ToString($md5.ComputeHash([System.IO.File]::ReadAllBytes($File1)))
    $Bait = [System.BitConverter]::ToString($md5.ComputeHash([System.IO.File]::ReadAllBytes($File2)))
    if ($Original -eq $Bait){
        Return $True
    }
    else{
        Return $False
    }
}
