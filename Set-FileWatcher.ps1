﻿function Set-FileWatcher {
<#
.SYNOPSIS
    This function will create file system watcher.

.INPUTS
    BaitFile
        Can be any file for md5 hash comparison.

.NOTES
    Version:        1.0
    Author:         Chris Taylor
                    christaylor.rocks
                    labtechconsulting.com
    Creation Date:  7/15/2016
    Purpose/Change: Initial script development

.EXAMPLE
    Set-FileWatcher -Path $Path -NotifyFilter FileName,LastWrite -Event Changed -Name Test -Action $Action

#>
        [CmdletBinding()]
        Param(
            [Parameter(Mandatory=$True,Position=0)]
            $Path,
            [Parameter(Mandatory=$true,Position=1)]
            [ValidateSet('Attributes','CreationTime','DirectoryName','FileName','LastAccess','LastWrite','Security','Size')]
            $NotifyFilter,
            [Parameter(Mandatory=$true,Position=2)]
            [ValidateSet('Changed','Created','Deleted','Error','Renamed')]
            $Event,
            [Parameter(Mandatory=$True,Position=3)]
            $Name,
            [Parameter(Mandatory=$True,Position=4)]
            $Action,            
            [switch]$Recurse

        )

        if($(Test-Path $Path) -eq $True -or $Event -eq 'Created'){
            
            $Folder = Split-Path -Path $Path
            $File = Split-Path $Path -Leaf

            $NewNotifyFilter = $($NotifyFilter.trim() -join ',')
            $Watcher = New-Object IO.FileSystemWatcher $Folder, $File -Property @{IncludeSubdirectories = $Recurse;NotifyFilter = [IO.NotifyFilters]$NewNotifyFilter}

            #Data
            $Data = new-object psobject -property @{Action = $Action}
            
            Try{
                Register-ObjectEvent $Watcher $Event -SourceIdentifier $Name -MessageData $Data -ErrorAction Stop -Action {Invoke-Expression "$($Event.MessageData.Action)"} | Out-Null
            }
            Catch{
                Write-Output "ERROR: $($error.exception[0].Message)"
                Exit
            }

        }
        else{Write-Output "ERROR: Unable to find file: $Path"}
} 



