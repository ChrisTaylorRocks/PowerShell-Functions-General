function Get-LatestFileDeletion {
    param(
        $File
    )

    function HasMatchingDeleteEvent{
        param( $Event )
        try{
            $DeleteHash = @{
                Logname = 'Security'
                ID = 4660
                StartTime = (Get-Date $Event.TimeCreated).AddSeconds(-1)
                EndTime = (Get-Date $Event.TimeCreated).AddSeconds(1)
            }
            $null = Get-WinEvent -FilterHashtable $DeleteHash -ErrorAction Stop
            $true
        }
        catch{ $false }
    }

    try{
        $Event = Get-WinEvent -FilterHashtable @{Logname='Security';ID=4663} | Where {
            $_.Properties[1].value -ne "$($env:COMPUTERNAME)$" -and
            $_.Properties[6].value -eq $File -and
            $(HasMatchingDeleteEvent -Event $_)
        } | select -First 1
    }catch{}

    [pscustomobject]@{
        Domain = $Event.Properties[2].Value
        Account = $Event.Properties[1].Value
        Path = $Event.Properties[6].value
        Time = $Event.TimeCreated
    }
}
