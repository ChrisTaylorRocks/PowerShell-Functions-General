﻿function Enable-FileAuditing {
<#
.SYNOPSIS
    This function will enable file system auditing.

.NOTES
    Version:        1.0
    Author:         Chris Taylor
                    christaylor.rocks
                    labtechconsulting.com
    Creation Date:  7/15/2016
    Purpose/Change: Initial script development

.EXAMPLE
    Enable-FileAuditing -Path C:\Temp -Principal Everyone -Permissions CreateFiles,Delete -Inheritance ObjectInherit -Propagation None -Flags Success

#>


    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$True,Position=0)]
        $Path,
        [Parameter(Mandatory=$True,Position=1)]
        [string]$Principal,
        [Parameter(Mandatory=$true,Position=2)]
        [ValidateSet('AppendData',`
                     'ChangePermissions',`
                     'CreateDirectories',`
                     'CreateFiles',`                
                    'AppendData',`	
                    'ChangePermissions',`	
                    'CreateDirectories',`
                    'CreateFiles',`
                    'Delete',`
                    'DeleteSubdirectoriesAndFiles',`	
                    'ExecuteFile',`
                    'FullControl',`	
                    'ListDirectory',`
                    'Modify',`
                    'Read',`
                    'ReadAndExecute',`
                    'ReadAttributes',`
                    'ReadData',`
                    'ReadExtendedAttributes',`
                    'ReadPermissions',`
                    'Synchronize',`
                    'TakeOwnership',`
                    'Traverse',`
                    'Write',`
                    'WriteAttributes',`
                    'WriteData',`
                    'WriteExtendedAttributes'
                    )]
        $Permissions,
        [Parameter(Mandatory=$true,Position=3)]
        [ValidateSet('ContainerInherit','None','ObjectInherit')]
        $Inheritance,
        [Parameter(Mandatory=$true,Position=4)]
        [ValidateSet('InheritOnly','None','NoPropagateInherit')]
        $Propagation,
        [Parameter(Mandatory=$true,Position=5)]
        [ValidateSet('Failure','None','Success')]
        $Flags

    )
 
    $NewPrincipal = $Principal
    $NewPermissions = $($Permissions.trim() -join ',')
    $NewInheritance = $Inheritance
    $NewPropagation = $Propagation
    $NewFlags = $Flags
        
    try{
        if($(Test-Path $Path) -eq $false) {
            Write-Output "ERROR: Path not found."
            return
        }
        
        $Auditpol = Auditpol /get /subcategory:"File System"
        if ($($Auditpol[4]) -notlike "*Success") {
            Auditpol /set /subcategory:"File System" /success:enable | Out-Null
        }

        $Auditpol = Auditpol /get /subcategory:"Detailed File Share"
        if ($($Auditpol[4]) -notlike "*Success") {
            Auditpol /set /subcategory:"Detailed File Share" /success:enable | Out-Null
        }

        
        $File_ACL = Get-Acl $Path
        $AccessRule = New-Object System.Security.AccessControl.FileSystemAuditRule("$Principal","$NewPermissions”,"$NewInheritance","$NewPropagation",”$NewFlags")
        $File_ACL.AddAuditRule($AccessRule)
        $File_ACL | Set-Acl $Path -ErrorAction Stop
    }
    catch{
        Write-Output "Error: $($_.Exception.Message)"
    }
    
    
}

